# Viz Objectstore Module #

## Requirements ##
Depends on oracledb module (https://www.npmjs.com/package/oracledb)

Oracledb module requires:

- Oracle instant client libraries version 11.2 or 12.1
- Node >=0.10.28 <0.11

## Usage Examples ##

	var objectstore = require('viz-objectstore')({
		user: OS_DB_USERNAME,
		password: OS_DB_PASSWORD,
		connectString: OS_DB_CONNECTSTRING
	});
	
	objectstore.getKeywords(function(error, keywords) {
	
		console.log(keywords);
	});
	
	var dateStart = new Date('May 17 2012 01:56:19 GMT-0400 (EDT)');
	var dateEnd = new Date('May 17 2015 01:56:19 GMT-0400 (EDT)');
	
	objectstore.search(
		{
			// set as many or as few parameters as needed
			keywords: ["FULLSCREEN HD"],
			query: "slug",
			dateStart: dateStart,
			dateEnd: dateEnd
		},
		function(error, images) {
			console.log(images);
		}
	);