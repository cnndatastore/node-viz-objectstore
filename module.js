/*
 * Created by jmurphy on 5/16/15
 */

module.exports = function(config) {
	return new ObjectstoreConn(config);
};

var path = require('path');
var utils = require(path.join(__dirname, "Utils.js"));
var oracledb = require('oracledb');

function ObjectstoreConn(config) {
	this.config = config;
}

ObjectstoreConn.prototype.connect = function(callback) {
	// lazy connect function (call when needed, not on module load)
	var self = this;
	if(self.connection)
		callback(null, this.connection);
	else {
		var credentials = utils.objectGet(self.config, [['user', 'password', 'connectString']]);

		oracledb.getConnection(credentials, function(error, connection) {
			if(error)
				callback(error, null);
			else {
				self.connection = connection;
				callback(null, connection);
			}
		});
	}
};

ObjectstoreConn.prototype.search = function(params, callback) {
	var query = "SELECT DISTINCT s.stillbilde_id, s.bildefilnavn, s.storrelse_x, s.storrelse_y, k.emneord, s.registrert_dato" +
		" FROM t_stillbilde s, t_stillbilde_emne k" +
		" WHERE s.stillbilde_id = k.stillbilde_id";

	if(typeof params.query === "string") // search string
		query += " AND UPPER(s.bildebeskrivelse) LIKE '%" + params.query.toUpperCase() + "%'"; // case insensitive

	if(typeof params.keywords === "string") // single keyword
		query += " AND k.emneord = '" + params.keywords  + "'";
	if(Array.isArray(params.keywords) && params.keywords.length > 0)
		query += " AND (k.emneord = '" + params.keywords.join("' OR k.emneord = '") + "')";

	if(params.dateStart instanceof Date)
		query += " and s.registrert_dato >= '" + dateFormat(params.dateStart) + "'";

	if(params.dateEnd instanceof Date) {
		var dateEndPlus = params.dateEnd;
		dateEndPlus.setDate(dateEndPlus.getDate() + 1);
		query += " AND s.registrert_dato <= '" + dateFormat(dateEndPlus) + "'"; // oracle uses midnight as the breakpoint, so add 24 hours to include the end day
	}

	query += " order by s.registrert_dato DESC"; // order by date descending

	this.execute(query, function(error, images) {
		if(error)
			callback(error, null);
		else {
			callback(null, groupImages(images));
		}
	});

	function groupImages(images) {
		// eliminate duplicate results by combining keywords into single entry
		var processed = [];
		var index = {};
		images.forEach(function (image) {
			var imageRemapped = remapImage(image);
			if(typeof index[imageRemapped.id] !== "undefined")
				Array.prototype.push.apply(processed[index[imageRemapped.id]].keywords, imageRemapped.keywords);
			else {
				processed.push(imageRemapped);
				index[imageRemapped.id] = processed.length - 1;
			}
		});
		return processed;
	}

	function remapImage(image) {
		return {
			id: image['STILLBILDE_ID'],
			filename: image['BILDEFILNAVN'],
			width: image['STORRELSE_X'],
			height: image['STORRELSE_Y'],
			keywords: [image['EMNEORD']],
			date: image['REGISTRERT_DATO']
		};
	}

	function dateFormat(date) {
		// format date in d-M-Y
		var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		return date.getDate() + "-" + months[date.getMonth()] + "-" + date.getFullYear();
	}
};

ObjectstoreConn.prototype.getKeywords = function(callback) {
	this.execute("SELECT UNIQUE emneord FROM t_stillbilde_emne", function(error, rows) {
		if(error)
			callback(error, null);
		else
			callback(null, utils.objectGet(rows, [null, 'EMNEORD']));
	});
};

ObjectstoreConn.prototype.execute = function(query, callback) {
	this.connect(function(error, connection) {
		if(error)
			callback(error, null);
		else {
			connection.execute(
				query,
				{}, /* From oracle docs: A bind variable parameter is needed to disambiguate the following options parameter; otherwise you will get Error: ORA-01036: illegal variable name/number */
				{outFormat: oracledb.OBJECT},
				function (error, result) {
					if (error)
						callback(error, null);
					else if (typeof result === "object" && typeof result.rows === "object")
						callback(null, result.rows);
					else
						callback("db_error", null);
				}
			);
		}
	});
};

ObjectstoreConn.prototype.disconnect = function(callback) {
	if(typeof this.connection !== "object") // not connected
		callback(null, true);
	else {
		this.connection.release(function(error) {
			if(error)
				callback(error);
			else
				callback(null, true);
		});
	}
};
